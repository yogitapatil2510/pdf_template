from django.shortcuts import render ,HttpResponse
from datetime import datetime
from home.models import Contact
from django.contrib import messages

# Create your views here.
def index(request):
    # context={
    #     'variable':'this  sent',
    #     'variable1':'this is',
    #     'variable2':'this is sent'
    # }
    # return HttpResponse("This is home page")
    # return render(request,'index.html',context)
    messages.success(request,"thi s test messaeg")
    return render(request,'index.html')

def about(request):
    # return render(request,'about.html')
     return render(request,'about.html')

def contact(request):
    if request.method =="POST":
        name=request.POST.get('name')
        mail=request.POST.get('mail')
        password=request.POST.get('password')
        desc=request.POST.get('desc')
        armyof=request.POST.get('armyof')
        contact=Contact(name=name,mail=mail,password=password,desc=desc,date=datetime.today())
        contact.save()
        messages.success(request,'Your Data has been send')
    # return HttpResponse("This is Contact page")
    return render(request,'contact.html')

def services(request):
    # return HttpResponse("This is services page")
     return render(request,'services.html')